import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.ojalgo.array.PrimitiveArray;
import org.ojalgo.finance.FinanceUtils;
import org.ojalgo.finance.portfolio.MarkowitzModel;
import org.ojalgo.finance.portfolio.PortfolioContext;
import org.ojalgo.matrix.BasicMatrix;
import org.ojalgo.matrix.PrimitiveMatrix;
import org.ojalgo.netio.BasicLogger;
import org.ojalgo.random.process.GeometricBrownianMotion;
import org.ojalgo.random.process.RandomProcess.SimulationResults;
import org.ojalgo.series.CalendarDateSeries;
import org.ojalgo.series.CoordinationSet;
import org.ojalgo.series.primitive.DataSeries;
import org.ojalgo.type.CalendarDateUnit;

import com.portfolio.QueryableYahooSymbol;

//http://luminouslogic.com/how-to-normalize-historical-data-for-splits-dividends-etc.htm
//https://greenido.wordpress.com/2009/12/22/yahoo-finance-hidden-api/
//http://advisorsoftware.com/portfolio-diagnostic-tool.html
public abstract class FinancialData {
	
	
	
	public static void main(final String[] args) throws IOException {
		
		List<String> portfolio = Arrays.asList(
				"EZTC3.SA", "ABEV3.SA", "EQTL3.SA",
				"BRFS3.SA",
				"CIEL3.SA",
				"GRND3.SA",
				"MDIA3.SA","ODPV3.SA", "PSSA3.SA", "RADL3.SA", "TOTS3.SA", "ITUB3.SA",
				"UGPA3.SA", 
				"VLID3.SA", "WEGE3.SA");
		Collections.sort(portfolio);
		
		/*YahooFinance.get("UGPA3.SA").getHistory(Interval.DAILY)
				.stream()
				.limit(10)
				.forEach(
						q-> System.out.println(String.format("%s Close:%f AdjClose:%f", q.getDate().getTime().toLocaleString(),q.getClose(), q.getAdjClose()))
				);*/
		
		final CoordinationSet<Double> tmpUncoordinated = new CoordinationSet<>();

		for(String asset : portfolio) {
			QueryableYahooSymbol symbol = new QueryableYahooSymbol(asset, CalendarDateUnit.DAY);
			tmpUncoordinated.put(asset,  symbol.getPriceSeries());
			
			System.out.println(String.format("%s %d", asset, symbol.getPriceSeries().size()));
		}

        
        final CoordinationSet<Double> tmpCoordinated = tmpUncoordinated.prune(CalendarDateUnit.WEEK);
        //double tmpYearsPerMonth = CalendarDateUnit.YEAR.convert(tmpCoordinated.getResolution());

        
        HashMap<String, Double> prices = new HashMap<String,Double>();
        BasicLogger.debug();
        BasicLogger.debug("Coordinated monthly data");
        Collection<CalendarDateSeries<Double>> timeSeriesCollection=  new ArrayList<CalendarDateSeries<Double>>(tmpCoordinated.size());
        //PrimitiveArray assetReturns = PrimitiveArray.make(portfolio.size()+1);
        //PrimitiveArray assetCAGR = PrimitiveArray.make(portfolio.size()+1);
        short index = 0;
        
        //BasicMatrix matrix = FinanceUtils.
        Set<Entry<String, CalendarDateSeries<Double>>> symbosDataSeries = tmpCoordinated.entrySet();
        //double periodInYears = tmpCoordinated.size() * 0.0191781;
		for (final Entry<String, CalendarDateSeries<Double>> tmpEntry : symbosDataSeries) {
            DataSeries timeSeries = tmpEntry.getValue().getDataSeries();
			
            timeSeriesCollection.add(tmpEntry.getValue());
            //prices.put( tmpEntry.getKey(), timeSeries.value(timeSeries.size()-1) );
            PrimitiveArray assetReturns = PrimitiveArray.make(portfolio.size()+1);
            for(int i = 0; i<tmpCoordinated.size(); i++){
            	double periodReturn = (timeSeries.value(timeSeries.size()-1) - timeSeries.value(0)) / timeSeries.value(0);
            }
            
            
			//assetReturns.add(index, periodReturn);
			//assetCAGR.add(index, cagr(timeSeries.value(timeSeries.size()-1), timeSeries.value(0), periodInYears));
            BasicLogger.debug("\tSeries: {} = {} \nReturn:", tmpEntry.getKey(), tmpEntry.getValue() );
            index++;
        }

		BasicMatrix covariance = FinanceUtils.makeCovarianceMatrix(timeSeriesCollection);
        
		PortfolioContext context = new PortfolioContext(
					FinanceUtils.toAssetVolatilities(covariance),  
					covariance
        		);
		
		
        MarkowitzModel mpt = new MarkowitzModel(context);
        for(int i = 0; i < tmpCoordinated.size(); i++) {
        	mpt.setUpperLimit(i, new BigDecimal("0.10"));
        	mpt.setLowerLimit(i, new BigDecimal("0.01"));
        }
        mpt.setRiskAversion(new BigDecimal("1000000"));
        mpt.setShortingAllowed(false);
        mpt.normalise();
        
        BasicMatrix tmpWgts = mpt.getAssetWeights();
        
        GeometricBrownianMotion forecast = mpt.forecast();
        
        double cash = 100000;
        Iterator<Entry<String, CalendarDateSeries<Double>>> iterator = symbosDataSeries.iterator();
        for (int i = 0; i < portfolio.size()-1; i++) {
        	
			String key = iterator.next().getKey();
			Number number = tmpWgts.get(i);
			System.out.println(String.format("%s\t%s", key, number));
		}
        
        //System.out.println(blm.getAssetWeights());
        
     // Comparing the month and year based stochastic processes for AAPL

        BasicLogger.debug();
        BasicLogger.debug("    Apple \t Monthly proc \t\t\t Annual proc \t\t (6 months from now)");
        BasicLogger.debug("Expected:   {} ", forecast.getDistribution(30).getExpected());
        BasicLogger.debug("StdDev:     {} ", forecast.getDistribution(30).getStandardDeviation());
        BasicLogger.debug("Var:        {} ", forecast.getDistribution(30).getVariance());
        BasicLogger.debug("Sharpe R.:  {} ", mpt.getSharpeRatio());
        BasicLogger.debug("Loss Pro.:  {} ", mpt.getLossProbability());
        BasicLogger.debug("VAR:  {} ", mpt.getValueAtRisk(BigDecimal.valueOf(0.9), 30));

      

        final SimulationResults tmpSimResults = forecast.simulate(2, 12, 1);
        BasicLogger.debug();
        BasicLogger.debug("Simulate future Google");
        BasicLogger.debug("Simulated sample set:  {}", tmpSimResults.getSampleSet(11));


        

    }

}
