package com.portfolio.model;

public enum TransactionType {

	BUY,
	SELL
}
