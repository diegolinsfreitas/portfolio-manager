package com.portfolio.model;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private TransactionType type;
	
	private Calendar date;
	
	private BigDecimal quantity;
	
	private BigDecimal price;
	
	private BigDecimal value;
	
	@ManyToOne
	private Asset asset;

	public void calcuateValue() {
		this.value = price.multiply(quantity);
	}

	



}
