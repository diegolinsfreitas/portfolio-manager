package com.portfolio.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
@NamedQuery(name="asset.by.symbol", query="select a from Asset a where a.symbol = :symbol")
public class Asset {
	
	public Asset() { }

	public Asset(String symbol) {
		this.symbol = symbol;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String symbol;

	private BigDecimal quantity = BigDecimal.ZERO;
	
	private BigDecimal meanPrice = BigDecimal.ZERO;
	
	private BigDecimal currentPosition = BigDecimal.ZERO;
	
	private BigDecimal target = BigDecimal.ZERO;
	
	private BigDecimal diff = BigDecimal.ZERO;
	private BigDecimal diffValue = BigDecimal.ZERO;
	
	
	private BigDecimal value = BigDecimal.ZERO;
	
	@ManyToOne(fetch=FetchType.LAZY)
	private Portfolio portfolio;

	public void addTransaction(Transaction transaction) {
		transaction.setAsset(this);
		transaction.calcuateValue();
		BigDecimal newQuantity = this.quantity.add(transaction.getQuantity());
		
		if(transaction.getType().equals(TransactionType.BUY)){
			this.value = value.add(transaction.getValue());
			this.meanPrice = this.value.divide(newQuantity);
			this.quantity = this.quantity.add(transaction.getQuantity());
		}else {
			this.quantity = this.quantity.subtract(transaction.getQuantity());
			this.value = value.multiply(this.quantity);
		}
	}

	public void calculateLastMarketValue(BigDecimal lastPrice) {
		this.value = lastPrice.multiply(this.quantity);
	}

	public void calcualtePositions() {
		this.currentPosition = this.value.divide(this.portfolio.getValue(),  3, RoundingMode.HALF_EVEN);
		this.diff = this.currentPosition.subtract(this.target);
		this.diffValue = this.diff.multiply(this.portfolio.getValue());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Asset other = (Asset) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}
	
	


	
}
