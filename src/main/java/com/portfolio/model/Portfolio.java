package com.portfolio.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.ws.rs.ServerErrorException;

import lombok.Data;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;

@Entity
@Data
public class Portfolio {
	
	public Portfolio() {
		
	}
	
	

	public Portfolio(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	@OneToMany(mappedBy="portfolio")
	private Set<Asset> assets;
	
	private BigDecimal value = BigDecimal.ZERO;


	public void calculateCurrentValueAndPositions() {
		if(assets.isEmpty()) {
			return;
		}
		this.value = BigDecimal.ZERO;
		final Map<String, Stock> stocks = new HashMap<String, Stock>(this.assets.size());
		
		Stream<Asset> assetStream = this.assets.stream();
		String[] symbols = assetStream
				.map(Asset::getSymbol).toArray(size -> new String[ size ]);
		
		try {
			stocks.putAll(YahooFinance.get(symbols));
		} catch (IOException e) {
			throw new ServerErrorException("GET STOCK PRICES", 500);
		} // single request
		
		assetStream = this.assets.stream();
		assetStream.forEach(a -> {
			a.calculateLastMarketValue(stocks.get(a.getSymbol()).getQuote().getPrice());
			this.value = this.value.add(a.getValue());
		});
	
		assetStream = this.assets.stream();	
		assetStream.forEach(a -> {
			a.calcualtePositions();
		});
		
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Portfolio other = (Portfolio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
}
