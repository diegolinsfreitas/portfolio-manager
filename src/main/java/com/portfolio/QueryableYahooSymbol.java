package com.portfolio;

import org.ojalgo.finance.data.YahooSymbol;
import org.ojalgo.type.CalendarDateUnit;

public class QueryableYahooSymbol extends YahooSymbol {

	public QueryableYahooSymbol(String symbol, CalendarDateUnit resolution) {
		super(symbol, resolution);
		addQueryParameter("a", "01");
		addQueryParameter("b", "01");
		addQueryParameter("c", "2010");
		addQueryParameter("f", "snd1l1yrr5");
		
	}
	
}