package com.portfolio.api;

import lombok.Data;

@Data
public class RestResponse<T> {

	public RestResponse(T data) {
		this.data = data;
	}
	private T data;
}
