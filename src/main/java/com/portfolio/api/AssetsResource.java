package com.portfolio.api;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.portfolio.model.Asset;
import com.portfolio.model.Portfolio;

@Stateless
@Path("portfolios/{idportfolio}/assets")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AssetsResource {
	
	@PersistenceContext
	private EntityManager entityManager;

	@GET
	public List<Asset> getList(@PathParam("idportfolio") long idPortfoliio) {
		
		Portfolio portfolio = (Portfolio) entityManager.createQuery("from Portfolio p left join fetch p.assets where p.id = "+ idPortfoliio).getSingleResult();
		
		Set<Asset> assets = portfolio.getAssets();
		
		portfolio.calculateCurrentValueAndPositions();
		
		List<Asset> result = new ArrayList<Asset>();
		result.addAll(assets);
		
	  
	    return result;

	}

}
