package com.portfolio.api;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;

import com.portfolio.model.Asset;
import com.portfolio.model.Transaction;
import com.portfolio.model.TransactionType;

@Provider
public class TransactionMessageBodyReader implements MessageBodyReader<Transaction> {

	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return type.equals(Transaction.class);
	}

	@Override
	public Transaction readFrom(Class<Transaction> type, Type genericType, Annotation[] annotations,
			MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException, WebApplicationException {
		Transaction mo = new Transaction();
		JsonParser parser = Json.createParser(entityStream);
		while (parser.hasNext()) {
			switch (parser.next()) {
			case KEY_NAME:
				String key = parser.getString();
				parser.next();
				switch (key) {
				case "id":
					mo.setId(parser.getLong());
					break;
				case "symbol":
					mo.setAsset(new Asset(parser.getString()));
					break;
				case "type":
					mo.setType(TransactionType.valueOf(parser.getString()));
					break;
				case "quantity":
					mo.setQuantity(BigDecimal.valueOf(Double.valueOf(parser.getString())));
					break;
				case "price":
					mo.setPrice(BigDecimal.valueOf(Double.valueOf(parser.getString())));
					break;
				case "date":
					Calendar date = Calendar.getInstance();
					try {
						date.setTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(parser.getString()));
					} catch (ParseException e) {
						throw new RuntimeException(e);
					}
					mo.setDate(date);
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
		}
		return mo;
	}

}
