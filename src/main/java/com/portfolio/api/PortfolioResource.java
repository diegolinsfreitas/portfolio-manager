package com.portfolio.api;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.portfolio.model.Portfolio;

@Stateless
@Path("portfolios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PortfolioResource {
	
	@PersistenceContext
	private EntityManager entityManager;

	@POST
	@Transactional
	public Portfolio savePortfolio(Portfolio portfolio) {
		entityManager.persist(portfolio);
		return portfolio;
		
	}
	 
	@GET
	public List<Portfolio> getList() {
		return entityManager.createQuery("select new Portfolio(id,name) from Portfolio").getResultList();
	}
	
	@DELETE
	@Path("{id}")
	@Transactional
	public void delete(@PathParam("id") long id){
		Portfolio portfolio = new Portfolio();
		portfolio.setId(id);
		entityManager.remove(portfolio);
	}

}
