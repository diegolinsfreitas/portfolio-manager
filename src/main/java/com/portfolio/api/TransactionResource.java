package com.portfolio.api;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.portfolio.model.Asset;
import com.portfolio.model.Portfolio;
import com.portfolio.model.Transaction;



/**
 * REST Service to expose the data to display in the UI grid.
 *
 * @author Roberto Cortez
 */
@Stateless
@Path("portfolios/{idportfolio}/transactions")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TransactionResource {
	
	@PersistenceContext
	private EntityManager entityManager;

	@POST
	@Transactional
	public void createTransaction(@PathParam("idportfolio") long idPortfoliio, Transaction transaction) {
		
		
		createAsset(idPortfoliio, transaction);
	}

	private void createAsset(long idPortfoliio, Transaction transaction) {
		Asset currentAsset = transaction.getAsset();
		currentAsset = getAssetForNewTransaction(idPortfoliio, transaction, currentAsset);
		
		currentAsset.addTransaction(transaction);
		
		entityManager.persist(transaction);
	}

	private Asset getAssetForNewTransaction(long idPortfoliio, Transaction transaction, Asset currentAsset) {
		Query query = entityManager.createNamedQuery("asset.by.symbol");
		query.setParameter("symbol", transaction.getAsset().getSymbol());
		try {
			currentAsset = (Asset) query.getSingleResult();
		} catch(NoResultException e) {
			currentAsset.setPortfolio(entityManager.getReference(Portfolio.class, idPortfoliio));
			entityManager.persist(currentAsset);
		}
		
		return currentAsset;
	}
	 
	@GET()
	@Transactional
	public List<Transaction> getList() {
		return entityManager.createQuery("from Transaction").getResultList();
	}

}
