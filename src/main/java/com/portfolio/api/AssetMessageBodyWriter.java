package com.portfolio.api;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import com.portfolio.model.Asset;



@Provider
@Produces(MediaType.APPLICATION_JSON)
public class AssetMessageBodyWriter implements MessageBodyWriter<List<Asset>>{

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {

		return type.isAssignableFrom(ArrayList.class)  
				&& genericType instanceof ParameterizedType
				&& ((Class)((ParameterizedType)genericType).getActualTypeArguments()[0]).isAssignableFrom(Asset.class);
		
		 
	}

	@Override
	public long getSize(List<Asset> t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public void writeTo(List<Asset> assets, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
			throws IOException, WebApplicationException {
		JsonGenerator gen = Json.createGenerator(entityStream);
		/*  gen.write("id", asset.getId())
		     .write("symbol", asset.getSymbol())
		     .write("quantity", asset.getQuantity())
		     .write("meanPrice",asset.getMeanPrice())
		     .write("currentPosition", asset.getCurrentPosition())
		     .write("target", asset.getTarget())
		     .write("diff", asset.getDiff())
		     .write("value", asset.getValue())
		     .writeEnd();
		  */
		  JsonGenerator array = gen.writeStartArray();
		  for(Asset asset : assets){
			  array.writeStartObject()
			     .write("id", asset.getId())
			     .write("symbol", asset.getSymbol())
			     .write("quantity", asset.getQuantity())
			     .write("meanPrice",asset.getMeanPrice())
			     .write("currentPosition", asset.getCurrentPosition())
			     .write("target", asset.getTarget())
			     .write("diff", asset.getDiff())
			     .write("value", asset.getValue())
			     .write("diffValue", asset.getDiffValue())
			     .writeEnd();
		  }
		  array.writeEnd();
		  
		  
		  gen.flush();
	}

}
