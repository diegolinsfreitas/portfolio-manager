package com.portfolio.api;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

@Stateless
@Path("quotes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class QuotesResource {

	@GET
	@Path("/symbols")
	public Response getQuotes(@QueryParam("query") String query) {
		Client client = ClientBuilder.newClient();
		String result = client.target(String.format("https://s.yimg.com/aq/autoc?query=%s&region=US&lang=en-US", query))
				.request().get().readEntity(String.class);

		CacheControl cc = new CacheControl();
		cc.setMaxAge(2000);
		cc.setNoStore(false);
		cc.setPrivate(true);

		ResponseBuilder builder = Response.ok(result);
		builder.cacheControl(cc);
		return builder.build();

	}

}
