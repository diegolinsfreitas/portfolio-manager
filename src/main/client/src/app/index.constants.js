(function ()
{
    'use strict';

    angular
        .module('fuse')
        .constant("config", {
        	
        		apiUrl: function(){
        			var url = "${server.url}";
        			if(url.indexOf("$") == 0) {
        				return "http://localhost:8080/mpt-1.0/resources/";
        			}else {
        				return url;
        			}
        			
        		}
	});
})();
