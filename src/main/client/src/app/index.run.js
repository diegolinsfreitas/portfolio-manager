(function ()
{
    'use strict';

    angular
        .module('fuse')
        .run(runBlock);

    /** @ngInject */
    function runBlock($rootScope, $timeout, $state)
    {
    	
    	
        // Activate loading indicator
        var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function ()
        {
            $rootScope.loadingProgress = true;
        });

        // De-activate loading indicator
        var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function ()
        {
            $timeout(function ()
            {
                $rootScope.loadingProgress = false;
            });
        });

        // Store state in the root scope for easy access
        $rootScope.state = $state;

        // Cleanup
        $rootScope.$on('$destroy', function ()
        {
            stateChangeStartEvent();
            stateChangeSuccessEvent();
        });
        

          // Initialize Firebase
          var config = {
            apiKey: "AIzaSyB8zsDc5G0CXgvwJu5q3PVI7xmlBSBXXwk",
            authDomain: "quad-6d5e8.firebaseapp.com",
            databaseURL: "https://quad-6d5e8.firebaseio.com",
            storageBucket: "quad-6d5e8.appspot.com",
          };
          firebase.initializeApp(config);
          $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
        	  // We can catch the error thrown when the $requireAuth promise is rejected
        	  // and redirect the user back to the home page
        	  if (error.code === "AUTH_REQUIRED") {
        	    $state.go("app.pages_auth_login");
        	  }
        	});
            
        //user.init({ appId: '561871756a59e' });
       
    }
})();