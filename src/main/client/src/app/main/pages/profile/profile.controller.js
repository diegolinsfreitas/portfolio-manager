(function ()
{
    'use strict';

    angular
        .module('app.profile')
        .controller('ProfileController', ProfileController);

    /** @ngInject */
    function ProfileController($mdDialog)
    {
        var vm = this;


        vm.basicForm = {};
       

        // Methods
        vm.sendForm = function (ev)
        {
        	vm.basicForm.user_id = 'self';
        	UserApp.User.save(vm.basicForm, function(error, result) {
                if (!error) {
                    // Check locks
                    if (result.locks && result.locks.length > 0 && result.locks[0].type == 'EMAIL_NOT_VERIFIED') {
                        callback && callback({ name: 'EMAIL_NOT_VERIFIED' }, result);
                        return;
                    } else {
                        // Success - Log in the user
                        //that.login(user);
                    }
                }

                callback && callback(error, result);
            });
        };
    }
})();