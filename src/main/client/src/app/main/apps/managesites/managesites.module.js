(function ()
{
    'use strict';

    angular
        .module('app.managesites', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.managesites', {
            url      : '/managesites',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/apps/managesites/managesite.html',
                    controller : 'ManageSitesController as vm'
                }
            },
            resolve  : {
                Tasks: function (msApi)
                {
                    return msApi.resolve('todo.tasks@get');
                },
                Tags : function (msApi)
                {
                    return msApi.resolve('todo.tags@get');
                }
            },
            bodyClass: 'todo'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/managesites');

        // Api
        msApiProvider.register('todo.tasks', ['app/data/todo/tasks.json']);
        msApiProvider.register('todo.tags', ['app/data/todo/tags.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('managesites', {
            title : 'Manage Sites',
            icon  : 'icon-sitemap',
            state : 'app.managesites',
        });
    }

})();