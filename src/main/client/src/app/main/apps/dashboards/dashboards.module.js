(function ()
{
    'use strict';

    angular
        .module('app.dashboards', [
            'app.dashboards.project'
            //,'app.dashboards.server',
            //'app.dashboards.analytics'
        ])
        .config(config);

    /** @ngInject */
    function config(msNavigationServiceProvider)
    {

        msNavigationServiceProvider.saveItem('dashboard', {
            title: 'Dashboard',
            icon  : 'icon-chart-line',
            state: 'app.dashboards_project'
        });

        /*msNavigationServiceProvider.saveItem('apps.dashboards.server', {
            title: 'Server',
            state: 'app.dashboards_server'
        });

        msNavigationServiceProvider.saveItem('apps.dashboards.analytics', {
            title: 'Analytics',
            state: 'app.dashboards_analytics'
        });*/
    }

})();