(function ()
{
    'use strict';

    angular
        .module('app.integration', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.integration', {
                url    : '/integration/javascript',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/apps/integration/javascript.html',
                        controller : 'JavaScriptController as vm'
                    }
                },
                resolve: {
                    SampleData: function (msApi)
                    {
                        return msApi.resolve('sample@get');
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/integration');

        // Api
        msApiProvider.register('sample', ['app/data/sample/sample.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('integration', {
            title : 'Integração',
            group : true,
            weight: 1
        });

        msNavigationServiceProvider.saveItem('integration.javascript', {
            title    : 'Javascript',
            icon     : 'icon-language-javascript',
            state    : 'app.integration',
            /*stateParams: {
                'param1': 'page'
             },*/
            translate: 'integration.javascript',
            weight   : 1
        });
        
        msNavigationServiceProvider.saveItem('integration.magento', {
            title    : 'Magento',
            icon     : 'icon-shopping',
            state    : 'app.integration',
            /*stateParams: {
                'param1': 'page'
             },*/
            translate: 'integration.magento',
            weight   : 1
        });
        
         msNavigationServiceProvider.saveItem('integration.shopify', {
            title    : 'Shopify',
            icon     : 'icon-shopping',
            state    : 'app.integration',
            /*stateParams: {
                'param1': 'page'
             },*/
            translate: 'integration.shopify',
            weight   : 1
        });
    }
})();