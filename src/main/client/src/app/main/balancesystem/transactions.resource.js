(function ()
{
    'use strict';

    angular
        .module('app.balancesystem')
        .factory('Transactions', _constructor);

    /** @ngInject */
    function _constructor($resource, config)
    {
 
    	
    	function _new( idPortifolio) {
    		var base_resource = $resource(config.apiUrl() + "portfolios/:idPortfolio/transactions/:id",{idPortfolio:idPortifolio});
    		this.query = base_resource.query;
    		this.save = base_resource.save;
    	}
    	
    	return _new;
    }
})();