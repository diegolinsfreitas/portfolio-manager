(function ()
{
    'use strict';

    angular
        .module('app.balancesystem')
        .factory('PortfolioResource', PortfolioResource);

    /** @ngInject */
    function PortfolioResource($resource, config)
    {
    	
    	var _this = $resource(config.apiUrl() + "portfolios/:id");    	

    	return _this;
    }
})();