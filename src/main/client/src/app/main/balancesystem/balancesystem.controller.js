(function ()
{
    'use strict';

    angular
        .module('app.balancesystem')
        .controller('BalanceSystemController', BalanceSystemController);

    /** @ngInject */
    function BalanceSystemController($scope,$rootElement, $mdSidenav,$mdBottomSheet, $timeout, $q, $log, $http, PortfolioResource,AssetsResource, Transactions, Quotes, auth)
    {
        var vm = this;
        
        
        vm.sortType     = 'diff'; // set the default sort type
        vm.sortReverse  = false;  // set the default sort order
        vm.searchFish   = '';     // set the default search/filter term
        vm.allocationHasChanged = false;
        vm.totalAllocation = 0;

        // Data
        vm.currentView = 'list';
        vm.showDetails = true;

        vm.assets = [];
        
        vm.selectedPortfolio = {};
        vm.selected = vm.assets[0];
        vm.portfolios = [];
        

        // Methods
        vm.select = select;
        vm.toggleDetails = toggleDetails;
        vm.toggleSidenav = toggleSidenav;
        vm.toggleView = toggleView;
        vm.addAsset = addAsset;
        
        

        //////////
        
        function addAsset($event) {
        	vm.selectedSymbol ='';
        	toggleDetails({
        		date: new Date(),
        		symbol: ''
        		
        	});
        }

        /**
         * Select an item
         *
         * @param item
         */
        function select(item)
        {
            vm.selected = item;
        }

        /**
         * Toggle details
         *
         * @param item
         */
        function toggleDetails(item)
        {
            vm.selected = item;
            toggleSidenav('details-sidenav');
        }
        
        vm.showTransactions = function() {
        	var parentEl = angular.element(document.getElementById('file-manager'));
        	$mdBottomSheet.show({
        	      templateUrl: 'app/main/balancesystem/views/transactions/transactions.html',
        	      controller: 'TransactionsController',
        	      clickOutsideToClose: true,
        	      parent: parentEl
        	    }).then(function(clickedItem) {
        	      // $scope.alert = clickedItem['name'] + ' clicked!';
        	    });
        }

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }

        /**
         * Toggle view
         */
        function toggleView()
        {
            vm.currentView = vm.currentView === 'list' ? 'grid' : 'list';
        }
        
        vm.buyAsset = function (form){
        	vm.form = form;
        	addTransaction("BUY")
        }
        
        vm.sellAsset = function (){
        	addTransaction("SELL")
        	vm.form = form;
        }
        
        function addTransaction(type) {
        	vm.transaction.symbol = vm.selectedSymbol.symbol
        	vm.transaction.type = type;
        	new Transactions(vm.selectedPortfolio.id).save(vm.transaction,function(data){
	    		toggleSidenav('details-sidenav');
	    		loadAssets();
	    		vm.selectedSymbol = null;
	    		vm.transaction = { };
	    		vm.form.$setPristine();
	    		vm.form.$setUntouched();
			    
            });
        }
        
       
        
        function loadAssets() {
        	if(vm.selectedPortfolio) {
        		vm.assets = new AssetsResource(vm.selectedPortfolio.id).query(function(){
        			//vm.selected = vm.assets[0];
        			markLowestSizeAsset();
        		});
        		
        	}
        	
        }
        
        function calculateTotalAllocation() {
        	var totalAllocation = 0;
        	for(var i = 0; i < vm.assets.length; i++) {
    			var asset = vm.assets[i];
    			totalAllocation += asset.target;
        	}
        	vm.totalAllocation = totalAllocation;
        }
        
        vm.updateTotalAllocation = function(){
        	calculateTotalAllocation();
        	vm.allocationHasChanged = true;
        }
        
        function markLowestSizeAsset(){
        	var lowestValue = null;
        	if(!(vm.assets.length == undefined || vm.assets.length == 0)) {
        		for(var i = 0; i < vm.assets.length; i++) {
        			var asset = vm.assets[i];
            		if(lowestValue  == null || asset.diff < lowestValue ){
            			lowestValue = asset.diff;
            			vm.lowestAssetValue = asset
            		}
            	}
        	}
        }
        
        $scope.$watch("vm.assets",markLowestSizeAsset);
        
        function loadPortfolios () {
        	vm.portfolios = PortfolioResource.query(function(){
        		vm.selectedPortfolio = vm.portfolios[0];
        		loadAssets()
        	});
        }
        
        vm.addPortfolio = function(){
        	PortfolioResource.save({'name':vm.portfolioName},function(result){
        			vm.portfolios.push(result);
	        		vm.portfolioName = '';
	        		vm.selectedPortfolio = result;
	        		loadAssets();
	        	});
        }
        
        vm.selectPortfolio = function(portfolio) {
        	vm.selectedPortfolio = portfolio;
        	loadAssets();
        }
        
        vm.assetDisabled    = false;
        // list of `state` value/display objects
        vm.quotes        = [];
        vm.querySearch   = Quotes.querySymbols;
        //vm.selectedItemChange = selectedItemChange;
        //vm.searchTextChange   = searchTextChange;
        
        // ******************************
        // Internal methods
        // ******************************
        /**
         * Search for quotes... use $timeout to simulate
         * remote dataservice call.
         */
       
        
        loadPortfolios();
    }
})();