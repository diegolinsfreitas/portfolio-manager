(function ()
{
    'use strict';

    angular
        .module('app.balancesystem')
        .controller('TransactionsController', _constructor);

    /** @ngInject */
    function _constructor($state)
    {
        var vm = this;

        // Data
        vm.products = [
                       { 
                           "id": 1,
                           "name": "Printed Dress",
                           "description": "Officia amet eiusmod eu sunt tempor voluptate laboris velit nisi amet enim proident et. Consequat laborum non eiusmod cillum eu exercitation. Qui adipisicing est fugiat eiusmod esse. Sint aliqua cupidatat pariatur mollit ad est proident reprehenderit. Eiusmod adipisicing laborum incididunt sit aliqua ullamco.",
                           "categories": [
                               "Women",
                               "Dresses"
                           ],
                           "tags": [
                               "dress",
                               "printed"
                           ],
                           "images": [
                               {
                                   "default": true,
                                   "id": 1,
                                   "url": "assets/images/ecommerce/product-image-placeholder.png",
                                   "type": "image"
                               }
                           ],
                           "priceTaxExcl": 100.00,
                           "priceTaxIncl": 110.00,
                           "taxRate": 10,
                           "comparedPrice": 119.90,
                           "quantity": 109,
                           "sku": "A445BV",
                           "width": "22cm",
                           "height": "24cm",
                           "depth": "15cm",
                           "weight": "3kg",
                           "extraShippingFee": 3.00,
                           "active": true
                       },
                       {
                           "id": 2,
                           "name": "Green Skirt",
                           "description": "Duis anim est non exercitation consequat. Ullamco ut ipsum dolore est elit est ea elit ad fugiat exercitation. Adipisicing eu ad sit culpa sint. Minim irure Lorem eiusmod minim nisi sit est consectetur.",
                           "categories": [
                               "Women",
                               "Dresses"
                           ],
                           "tags": [
                               "dress",
                               "printed"
                           ],
                           "images": [
                               {
                                   "default": true,
                                   "id": 1,
                                   "url": "assets/images/ecommerce/product-image-placeholder.png",
                                   "type": "image"
                               }
                           ],
                           "priceTaxExcl": 100.00,
                           "priceTaxIncl": 110.00,
                           "taxRate": 10,
                           "comparedPrice": 119.90,
                           "quantity": 109,
                           "sku": "A445BV",
                           "width": "22cm",
                           "height": "24cm",
                           "depth": "15cm",
                           "weight": "3kg",
                           "extraShippingFee": 3.00,
                           "active": true
                       }];

        vm.dtInstance = {};
        vm.dtOptions = {
            dom         : 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            columnDefs  : [
                {
                    // Target the id column
                    targets: 0,
                    width  : '72px'
                },
                {
                    // Target the image column
                    targets   : 1,
                    filterable: false,
                    sortable  : false,
                    width     : '80px'
                },
                {
                    // Target the quantity column
                    targets: 5,
                    render : function (data, type)
                    {
                        if ( type === 'display' )
                        {
                            if ( parseInt(data) <= 5 )
                            {
                                return '<div class="quantity-indicator md-red-500-bg"></div><div>' + data + '</div>';
                            }
                            else if ( parseInt(data) > 5 && parseInt(data) <= 25 )
                            {
                                return '<div class="quantity-indicator md-amber-500-bg"></div><div>' + data + '</div>';
                            }
                            else
                            {
                                return '<div class="quantity-indicator md-green-600-bg"></div><div>' + data + '</div>';
                            }
                        }

                        return data;
                    }
                },
                {
                    // Target the status column
                    targets   : 6,
                    filterable: false,
                    render    : function (data, type)
                    {
                        if ( type === 'display' )
                        {
                            if ( data === 'true' )
                            {
                                return '<i class="icon-checkbox-marked-circle green-500-fg"></i>';
                            }

                            return '<i class="icon-cancel red-500-fg"></i>';
                        }

                        if ( type === 'filter' )
                        {
                            if ( data )
                            {
                                return '1';
                            }

                            return '0';
                        }

                        return data;
                    }
                },
                {
                    // Target the actions column
                    targets           : 7,
                    responsivePriority: 1,
                    filterable        : false,
                    sortable          : false
                }
            ],
            initComplete: function ()
            {
                var api = this.api(),
                    searchBox = angular.element('body').find('#e-commerce-products-search');

                // Bind an external input as a table wide search box
                if ( searchBox.length > 0 )
                {
                    searchBox.on('keyup', function (event)
                    {
                        api.search(event.target.value).draw();
                    });
                }
            },
            pagingType  : 'simple',
            lengthMenu  : [10, 20, 30, 50, 100],
            pageLength  : 20,
            scrollY     : 'auto',
            responsive  : true
        };

        // Methods
        vm.gotoProductDetail = gotoProductDetail;

        //////////

        /**
         * Go to product detail
         *
         * @param id
         */
        function gotoProductDetail(id)
        {
            $state.go('app.e-commerce.products.detail', {id: id});
        }
    }
})();