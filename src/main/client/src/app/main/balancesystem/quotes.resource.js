(function ()
{
    'use strict';

    angular
        .module('app.balancesystem')
        .factory('Quotes', _constructor);

    /** @ngInject */
    function _constructor($http,$q, config)
    {
    	var _this = this;

    	_this.querySymbols = function  (query) {
            var deferred = $q.defer();
            $http.get(config.apiUrl() + 'quotes/symbols?query='+query).success(function(data){
            	 deferred.resolve( data.ResultSet.Result);
            });
            return deferred.promise;
        }
    	
    	return _this;
    }
})();