(function ()
{
    'use strict';

    angular
        .module('app.balancesystem', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.balancesystem', {
            url      : '/management',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/balancesystem/balancesystem.html',
                    controller : 'BalanceSystemController as vm'
                }
            },
            resolve: {
            	// controller will not be loaded until $requireAuth resolves
                // Auth refers to our $firebaseAuth wrapper in the example above
                "auth": ['$q', function($q) {
                  // $requireAuth returns a promise so the resolve waits for it to complete
                  // If the promise is rejected, it will throw a $stateChangeError (see above)
                	//var ref = new Firebase("https://quad-6d5e8.firebaseio.com");
      	      	  // create an instance of the authentication service
                	
                	firebase.auth().onAuthStateChanged(function(user) {
	
	                	if (user) {
	                		$q.resolve(user);
	                	} else {
	                		 var errorObject = { code: 'AUTH_REQUIRED' };
	                         return $q.reject(errorObject);
	                	}
                	});
                	
                	return $q
                
                }]
            },
            
            bodyClass: 'file-manager'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/balancesystem');

        // Api
        //msApiProvider.register('api.portfolios', ['resources/portfolios']);

        // Navigation
        msNavigationServiceProvider.saveItem('balancesystem', {
            title : 'Gerenciamento de Posições',
            icon  : 'icon-folder',
            state : 'app.balancesystem',
            weight: 4
        });
    }

})();