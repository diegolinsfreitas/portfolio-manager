(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [
            'ngMdIcons',
            'ui.utils.masks',

            // Core
            'app.core',

            // Navigation
            'app.navigation',

            // Toolbar
            'app.toolbar',

            // Quick panel
            'app.quick-panel',
            
            
            'app.pages.auth.register',
            
            'app.pages.auth.login',
            
            'app.dashboards', 
            'app.profile',
            'app.managesites',
            'app.integration',
            'app.balancesystem'
        ]);
})();